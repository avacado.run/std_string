#include <iostream>
#include <string>

int main(int argc, char* argv[])
{
    std::cout << "Input new string: ";
    std::string newString;
    std::getline(std::cin, newString);
    
    std::cout << "String: " << newString << std::endl
    << "Length: " << newString.length() << std::endl
    << "First element: " << newString[0] << std::endl
    << "Last element: " << newString[newString.length() - 1] << std::endl;
    
    return 0;
}
